$.fn.asproTextConstruct = function( settings ){
  /**
   * Переменная необходима для правильной работы форматирования в IE
   * @type {boolean}
   */
  var _onPaste_Strip = false;

  /**
   * Shortcut для preventDefault()
   * @param {Event} e - получает событие
   */
  var prevent = function(e) {
      e.preventDefault();
  };

  var settings = $.extend({
      inputParentSelect:  ".input-box",  // селектор элемента, в который должна вставляться строка (относительно текущего элемента) (пустая строка - текущий элемент)
      inputClass:   "input-box-field",   // класс инпута с текстом
      inputItemClass: "input-item",
      placeholder:  "",                  // placeholder для пустой строки
      autocomplete: false,               // наличие атрибута autocomplete
      spellcheck:   true,                // наличие атрибута spellcheck
      autofocus:    true,                // наличие атрибута autofocus
      typefocus:    true,                // автофокус в строку при печатании
      pastecheck:   true,                // форматирование при вставке контента в строку
      tabindex:     "-1",                // значение атрибута tabindex
      maxlength:    1000,                // максимальная длина контента в строке

      selectSelect: ".menu",             // селектор элемента, в который должен вставляться список (относительно текущего элемента)
      selectWrapperClass: 'menu-wrapper',
      selectTitleClass: 'menu-wrapper-title',
      selectItemClass: 'menu-item',
      selectTextBlockClass: 'textBlock',
      selectData:   {},                  // данные списка
      textBlock: {
        title: 'Текстовый блок',
      },

      view: 'html',
  }, settings);

  settings.selectors = {
    selectItems: settings.selectSelect + " ." + settings.selectWrapperClass + " ." + settings.selectItemClass,
    inputItems: "." + settings.inputClass + " ." + settings.inputItemClass,
  };

  var requieredProps = [
      'inputClass',
      'selectSelect',
      'selectWrapperClass',
      'selectItemClass',
      'selectTextBlockClass',
  ];

  var error = errorsCheck(settings, requieredProps);
  if( !error.result ) {
      console.log(error.message);
      return false;
  }
  
  var el = $(this);
  if(el.length){
    init(el);
  }

  function init(el){
      // подготовка атрибутов autocomplete и spellcheck
      settings.autocomplete = (settings.autocomplete) ? "on" : "off";
      settings.spellcheck = (settings.spellcheck) ? "true" : "false";

      settings.addEvents = true;

      el.each(function() {
          _this = $(this);

          // поиск начальных элементов
          var inputBox = settings.inputParentSelect ? _this.find(settings.inputParentSelect) : _this;
          var selectBox = _this.find(settings.selectSelect);

          if( !inputBox.length || !selectBox.length ) {
            console.log('Не найден inputBox или selectBox');
            return false;
          }

          // генерация поля ввода
          inputBox.attr("contenteditable","false");
          inputBox.prepend('<div class="changeView">VIEW</div>');
          inputBox.prepend('<div class="' + settings.inputClass + '" tabindex="' + settings.tabindex + '" autocomplete="' + settings.autocomplete + '" spellcheck="' + settings.spellcheck + '" _moz_resizing="false" contenteditable></div><div class="' + settings.inputClass + '-placeholder" data-ph="' + settings.placeholder + '" contenteditable="false"></div>');
      
          // блок для ввода текста
          if(settings.textBlock !== undefined && settings.textBlock.title !== undefined) {
              let selectTextBlockHTML = $("<div class='" + settings.selectWrapperClass + "'><div class='" + settings.selectItemClass + " " + settings.selectTextBlockClass + "' data-code=''>"+settings.textBlock.title+"</div></div>");
              selectBox.append(selectTextBlockHTML);
              selectTextBlockHTML = undefined;
          }

          // генерация списка
          generateSelectBox(settings, selectBox);

          // запись в дату для удобства
          var elementsData = {
            _this: _this,
            fieldBox: inputBox,
            field: inputBox.find('.' + settings.inputClass),
            fieldPh: inputBox.find('.' + settings.inputClass + '-placeholder'),
            selectBox: selectBox,
            selectWrappers: selectBox.find('.' + settings.selectWrapperClass),
            selectItems: selectBox.find('.' + settings.selectItemClass),
          };
          var constructData = {
              settings: settings,
              elements: elementsData,
              functions: addFunctions(elementsData, settings),
          };
          _this.data('asproTextConstruct', constructData);

          // добавление обработчиков инпута
          addInputEvents(constructData);

          // добавление обработчиков списка
          addSelectEvents(constructData);

          // добавление обработчиков drag and drop
          addDragEvents(constructData);
          settings.addEvents = false;
          
      });

  }
  
  function generateSelectBox(settings, selectBox) {
      if(settings.selectData.length) {
          settings.selectData.forEach((element, parentIndex) => {
              var bFunction = element.type !== undefined && element.type == 'functions';
              let selectBoxWrapHTML = $("<div class='" + settings.selectWrapperClass + "'><div class='" + settings.selectTitleClass + "'>"+element.title+"</div></div>");
              selectBox.append(selectBoxWrapHTML);
              if(element.childs.length) {
                  var menuWrapper = selectBox.find("."+settings.selectWrapperClass+":last-of-type");
                  element.childs.forEach((child, childIndex) => {
                      let selectItemHTML = $('<div class="' + settings.selectItemClass + '" ' + (bFunction ? ' data-type="function" ' : '') + "data-title='"+child.title+"' data-code='"+child.code+"' data-parent-index='"+parentIndex+"' data-child-index='"+childIndex+"'>"+child.title+"</div>");
                      menuWrapper.append(selectItemHTML);
                  });
              }
          });        
      }
  }

  function addInputEvents(constructData) {
      var input = constructData.elements.field;
      input.on("drag", prevent);
      input.on("drop", prevent);
      input.on("resize", prevent);
      input.on("click", function() {// определение текущего положения каретки
          var sel = window.getSelection();
          var setRange = 
            sel 
            && sel.anchorNode !== undefined 
            && $(sel.anchorNode).closest('.' + constructData.settings.inputClass).length
            && !$(sel.anchorNode).closest('.' + constructData.settings.selectTextBlockClass).length;
          if ( setRange ) {
              if (sel.getRangeAt && sel.rangeCount) {
                  var range = sel.getRangeAt(0);
                  constructData.settings.inputRange = range;
              }
          }
      });

      constructData.elements.fieldBox.find('.changeView').on("click", function() {
        constructData.functions.switchView();
      });

      input.on("keypress", function(e) {
          if( !$(window.getSelection().anchorNode).closest('.' + constructData.settings.selectTextBlockClass).length && input.attr('view') != 'text' ) {
              e.preventDefault();
          }
      });

      if (constructData.settings.autofocus) constructData.functions.setCaret();
      if (constructData.settings.pastecheck) input.on("paste", function(e) {
          constructData.functions.format(e, constructData.settings.maxlength);
      });
      if (constructData.settings.typefocus) {
          $(document).find(constructData.elements._this).on("keydown", function(e) {
              if (document.activeElement !== input[0]) constructData.functions.setCaret();
              e.stopPropagation();
          });
      }
  
      // отключение возможности resize для строки
      if(document.queryCommandSupported("enableObjectResizing")) document.execCommand("enableObjectResizing", false, false);
  
      // удаление лишних дочерних <br>, которые добавляет в строку Firefox
      if ("MozAppearance" in document.documentElement.style) {
          input.on("keyup", function(e) {
              if (input[0].textContent.length < 2 && e.key === "Backspace") {
                  constructData.functions.clear("br");
                  input[0].innerHTML = input[0].innerHTML.trim();
                  constructData.functions.setCaret(input[0]);
              }
              e.stopPropagation();
          });
      }
  }

  function addSelectEvents(constructData) {
  
      if(constructData.elements.selectItems.length) {

          constructData.elements._this.find(constructData.settings.selectors.selectItems).on('click', function() {
              var _this = $(this);
              var dragBox = constructData.functions.createDrag(_this);
              if(dragBox) {
                constructData.functions.insert(dragBox);
              }            
          });
  
          $(document).find(constructData.elements._this).on('click', constructData.settings.selectors.inputItems + " .itemBody", function(e) {
              var inputItem = $(this).closest('.' + constructData.settings.inputItemClass);
              
              if( !inputItem.hasClass('clicked') && $(e.target).hasClass('itemBody') ) {
                var parentIndex = inputItem.data('parentIndex');
                var childIndex = inputItem.data('childIndex');
                var inputItemSelect = '';

                if(inputItem.data('type') == 'function') {
                  inputItemSelect += '<div class="selectWrapperFunction">';

                  constructData.settings.selectData.forEach(function(parentEl, index) {
                    if( index != parentIndex ) {
                      if(parentEl.childs.length) {
                        inputItemSelect += '<div class="selectWrapper">';

                        inputItemSelect += '<div class="selectWrapperTitle">';
                          inputItemSelect += parentEl.title;
                        inputItemSelect += '</div>';

                        inputItemSelect += '<div class="selectWrapperBody">';
                          parentEl.childs.forEach(function(el, i){
                            inputItemSelect += '<div class="selectItem" data-code="'+el.code+'" data-title="'+el.title+'" data-child-index="'+i+'" data-parent-index="'+parentIndex+'">'+el.title+'</div>';
                          });
                        inputItemSelect += '</div>';

                        inputItemSelect += '</div>';
                      }
                    }
                  });

                  inputItemSelect += '</div>';
                } else {
                  var parentData = constructData.settings.selectData[parentIndex];
                  if(parentData !== undefined && parentData.childs.length > 1) {
                      inputItemSelect += '<div class="selectWrapper">';
                      parentData.childs.forEach(function(el, i){
                          if(i != childIndex) {
                              inputItemSelect += '<div class="selectItem" data-code="'+el.code+'" data-title="'+el.title+'" data-child-index="'+i+'" data-parent-index="'+parentIndex+'">'+el.title+'</div>';
                          }
                      });
                      inputItemSelect += '</div>';
                  }
                }
                
                if(inputItemSelect) {
                  inputItemSelect = $(inputItemSelect);
                  inputItemSelect.find('.selectWrapper').data('targetItem', inputItem);
                  var offset = this.getBoundingClientRect();
                  inputItemSelect.css({
                      'position': 'fixed',
                      'top': offset.bottom,
                      'left': offset.left,
                  });
                  $('body').append(inputItemSelect);
                  inputItem.addClass('clicked');
                  e.stopPropagation();
                }
              }
          });
  
          $(document).find(constructData.elements._this).on('click', constructData.settings.selectors.inputItems + " .deleteItem", function() {
            var inputItem = $(this).closest('.' + constructData.settings.inputItemClass);
            var input = $(this).closest('.' + constructData.settings.inputClass);
            var nodes = input[0].childNodes;
  
            nodes.forEach(function(node, i) {
              if(node == inputItem[0]) {
                var nodesToRemove = [];
                if(nodes[i-1] !== undefined && nodes[i-1].textContent == ' ') {
                  nodesToRemove.push(nodes[i-1]);
                }
                if(nodes[i+1] !== undefined && nodes[i+1].textContent == ' ') {
                  nodesToRemove.push(nodes[i+1]);
                }
                nodesToRemove.push(node);
                nodesToRemove.forEach(function(el){
                  el.remove();
                });
              }
            });
          });

          $(document).find(constructData.elements._this).on('click', constructData.settings.selectors.inputItems + " .deleteItemFunction", function() {
            var inputItem = $(this).closest('.' + constructData.settings.inputItemClass);
            var _this = $(this).closest('.functionItemInside');

            var inputCode = inputItem.attr('data-code');
            var index = _this.index();

            inputCode = inputCode.slice(1, -1).split(' ');
            inputCode.splice(index + 1, 1);
            inputCode = '{' + inputCode.join(' ') + '}';
            inputItem.attr('data-code', inputCode);

            _this.remove();
          });

          $(document).on('click', function(e) {
            var selectWrapper = $('.selectWrapper, .selectWrapperFunction');
            if( selectWrapper.length ) {
                var target = $(e.target);
                if( !target.closest('.selectWrapper, .selectWrapperFunction').length ) {
                    selectWrapper.remove();
                    $("." + constructData.settings.inputItemClass).removeClass('clicked');
                }
            }
          });
  
          $(document).on('click', ".selectWrapper .selectItem", function() {
              var _this = $(this);
              var wrapper = _this.closest('.selectWrapper');
              var targetItem = wrapper.data('targetItem');
              var title = _this.data('title');
              var code = _this.data('code');
              var parentIndex = _this.data('parentIndex');
              var childIndex = _this.data('childIndex');
  
              if(targetItem !== undefined) {
                if(_this.closest('.selectWrapperFunction').length) {
                  var targetCode = targetItem.attr('data-code');
                  targetCode = targetCode.slice(0, -1) + ' ' + code + '}';
                  targetItem.attr('data-code', targetCode);

                  var targetBody = targetItem.find('.itemBody');

                  var functionItemInsideHTML = '<span class="functionItemInside" data-code="' + code + '">' + title + '<span class="deleteItemFunction">x</span></span>';
                  targetBody.append(functionItemInsideHTML);
                } else {
                  targetItem.attr('data-title', title);
                  targetItem.attr('data-code', code);
                  targetItem.attr('data-parent-index', parentIndex);
                  targetItem.attr('data-child-index', childIndex);
                  targetItem.find('.itemBody').text(title);
                }

                wrapper.remove();
                $(constructData.settings.selectors.inputItems).removeClass('clicked');
              }
          });
      }
  }

  function addDragEvents(constructData) {
        $(document).find(constructData.elements._this).on('mousedown', '.dragBox', function(event) {
          var _this = $(this);
          var _clone = false;
          var append = true;
          let shiftX = event.clientX - this.getBoundingClientRect().left;
          var startX = event.pageX - shiftX;
          var startTop = _this.offset().top;
          
          var childBounds = _this[0].getBoundingClientRect();
          var parentBounds = constructData.elements.fieldBox[0].getBoundingClientRect();
            
          var maxLeft = parentBounds.right - childBounds.width;
          var minLeft = parentBounds.left;
      
          function moveAt(pageX) {
              if(append) {
                _clone = _this.clone();
                _clone.insertAfter(_this).addClass('draging');
                $('body').append(_this);
                _this.addClass('dragingOut');
                append = false;
              }
              
              var left = pageX - shiftX > maxLeft ? maxLeft : ( pageX - shiftX < minLeft ? minLeft : pageX - shiftX );
      
              _this.css({
                  position: 'absolute',
                  cursor: 'pointer',
                  'z-index': '1000',
                  left: left + 'px',
                  top: startTop + 'px',
              });
      
              var input = _clone.closest('.' + constructData.settings.inputClass)[0];
              clearNodes(input);
      
              var nodes = input.childNodes;
              nodes.forEach(function(node, i) {
                if(node == _clone[0]) {
      
                  if(left > startX) { // move right
                    var targetItem = $(node).next();
                    if(targetItem.length) {
                      var distance = targetItem.outerWidth(true);
                      if(left - startX >= distance) {
                        // move item
                        var spaceBefore = document.createTextNode("\u00A0");
                        var spaceAfter = document.createTextNode("\u00A0");
                        var fragment = document.createDocumentFragment();
                        fragment.appendChild(spaceBefore);
                        fragment.appendChild(spaceAfter);
                        fragment.appendChild(node);
      
                        var nodesToRemove = [];
                        if(nodes[i-1] !== undefined && nodes[i-1].textContent == ' ') {
                          nodesToRemove.push(nodes[i-1]);
                        }
                        if(nodes[i+1] !== undefined && nodes[i+1].textContent == ' ') {
                          nodesToRemove.push(nodes[i+1]);
                        }
                        nodesToRemove.forEach(function(el){
                          el.remove();
                        });
      
                        $(fragment).insertAfter(targetItem);
                        startX = left;
                      }
                    }
                  } else if(left < startX) { // move left
                    var targetItem = $(node).prev();
                    if(targetItem.length) {
                      var distance = targetItem.outerWidth(true);
                      if(startX - left >= distance) {
                        // move item
                        var spaceBefore = document.createTextNode("\u00A0");
                        var spaceAfter = document.createTextNode("\u00A0");
                        var fragment = document.createDocumentFragment();
                        fragment.appendChild(node);
                        fragment.appendChild(spaceBefore);
                        fragment.appendChild(spaceAfter);
      
                        var nodesToRemove = [];
                        if(nodes[i-1] !== undefined && nodes[i-1].textContent == ' ') {
                          nodesToRemove.push(nodes[i-1]);
                        }
                        if(nodes[i+1] !== undefined && nodes[i+1].textContent == ' ') {
                          nodesToRemove.push(nodes[i+1]);
                        }
                        nodesToRemove.forEach(function(el){
                          el.remove();
                        });
      
                        $(fragment).insertBefore(targetItem);
                        startX = left;
                      }
                    }
                  }
                }
              });
      
      
          }
      
          function onMouseMove(event) {
              moveAt(event.pageX);
          }
      
          document.addEventListener('mousemove', onMouseMove);
      
          $(document).on('mouseup', function() {
              if(_clone) {
                _clone.removeClass('draging');
                _this.remove();
              }
              document.removeEventListener('mousemove', onMouseMove);
              this.onmouseup = null;
          });
              
          function clearNodes(el) {
            if( el !== undefined && el.childNodes !== undefined ) {
              var nodes = el.childNodes;
              nodes.forEach(function(node, i) {
                if(node.length !== undefined && !node.length) {
                  nodes[i].remove();
                }
              });
            }
          }
      
          this.ondragstart = function() {
              return false;
          };
      });
  }

  function addFunctions(elementsData, settings) {
    var functions = {
      /**
       * Функция установки каретки в необходимую позицию
       * @param {Object} el - необходимый элемент
       * @param {String} [place] - положение каретки
       */
      setCaret: function(el,place) {
        el = el || elementsData.field[0];
        var range = document.createRange();
        var sel = function() {
          var s = window.getSelection();
          s.removeAllRanges();
          s.addRange(range);
        };
        switch(place) {
          case "after": // после указанного элемента
            range.setStartAfter(el);
            range.setEndAfter(el);
            if(document.getSelection) sel();
            break;
          case "select": // выделение всего контента в элементе
            range.selectNodeContents(el);
            sel();
            break;
          default: // вставка в элемент после контента
            el.focus();
            range.selectNodeContents(el);
            range.collapse(false);
            sel();
        }
      },
      /**
       * Функция форматирования контента при вставке в поле
       * @param {Event} e - событие
       * @param {Number} maxlength - максимальная длина контента
       */
      format: function(e, maxlength) {
        if (e.originalEvent && e.originalEvent.clipboardData && e.originalEvent.clipboardData.getData) {
          e.preventDefault();
          var text = e.originalEvent.clipboardData.getData("text/plain");
          if (text.length > maxlength) text = text.substring(0, maxlength);
          document.execCommand("insertText", false, text);
        } else if (e.clipboardData && e.clipboardData.getData) {
          e.preventDefault();
          var text = e.clipboardData.getData("text/plain");
          if (text.length > maxlength) text = text.substring(0, maxlength);
          document.execCommand("insertText", false, text);
        } else if (window.clipboardData && window.clipboardData.getData) {
          if (!_onPaste_Strip) {
            _onPaste_Strip = true;
            e.preventDefault();
            document.execCommand("ms-pasteTextOnly", false);
          }
          _onPaste_Strip = false;
        }
      },
      /**
       * Функция вставки div в строку
       */
      insert: function(element) {
        var input = elementsData.field,
            spaceBefore = document.createTextNode("\u00A0"),
            spaceAfter = document.createTextNode("\u00A0");
 
        // вставка div в опредленное кареткой место (если поддерживается браузером)
        if(settings.inputRange !== undefined) {
          settings.inputRange.insertNode(spaceBefore);
          settings.inputRange.insertNode(element[0]);
          settings.inputRange.insertNode(spaceAfter);
          settings.inputRange = undefined;
          return true;
        }
    
        // Стандартная вставка элемента, если положение каретки не задано
        input.append(spaceBefore);
        input.append(element);
        input.append(spaceAfter);
        functions.setCaret();
        if(settings.inputRange !== undefined) {
          settings.inputRange = undefined;
        }
      },
      /**
       * Функция очистки поля
       * @param {String} tag - удаляемый тег
       */
      clear: function(tag) {
        var el = elementsData.field;
        if(tag) {
          el.find(tag).remove();
        } else {
            el.find('*').remove();
        };
        functions.setCaret();
      },
      createDrag: function(element) {
        _element = $(element);
        var title = _element.data('title');
        var code = _element.data('code');
        var parentIndex = _element.data('parentIndex');
        var childIndex = _element.data('childIndex');
        var text = _element.data('text');
        var type = _element.data('type');

        if(_element.hasClass(settings.selectTextBlockClass)){
            var dragBox = $('<div class="dragBox ' + settings.inputItemClass + ' ' + settings.selectTextBlockClass + '" data-code=""><span class="itemBody" contenteditable>&nbsp;' + (text ? text : '') + '&nbsp;</span><span class="deleteItem" contenteditable="false">x</span></div>');
            return dragBox;
        } else if(title !== undefined && code !== undefined) {
            var dragBox = $('<div class="dragBox ' + settings.inputItemClass + '" ' + (type == 'function' ? ' data-type="function" ' : '') + 'data-code="{='+code+'}" data-child-index="'+childIndex+'" data-parent-index="'+parentIndex+'" contenteditable="false"><span class="itemBody">'+title+(type == 'function' ? ':' : '')+'</span><span class="deleteItem">x</span></div>');
            return dragBox;
        } else {
            return false;
        }
      },
      switchView: function() {
        if(settings.view == 'html') {
          var items = elementsData.field.find('.' + settings.inputItemClass);
          var result = '';
          if(items.length) {
            items.each(function(index, item) {
              _item = $(item);

              if( _item.hasClass(settings.selectTextBlockClass) ) {
                var itemText = _item.text().trim().slice(0, -1);
                result += itemText;
              } else {
                result += _item.attr('data-code');
              }
            });
          }

          elementsData.field.text(result);
          elementsData.field.attr('view', 'text');
          settings.view = 'text';
        } else if(settings.view == 'text') {
          var inputText = elementsData.field.text();
          inputText = inputText.split('{=');
          //inputText = Array.from( inputText.matchAll(/\{\=(.*?)\}/g) );
          elementsData.field.text('');
          inputText.forEach(function(item) {
            if(settings.selectData.length) {
              var findMatch = false;
              settings.selectData.forEach((element, parentIndex) => {
                var bFunction = element.type !== undefined && element.type == 'functions';
                if(element.childs.length) {
                  element.childs.forEach((child, childIndex) => {
                    item = item.replace('}', '');
                    var needCreate = false;
                    if( child.code == item ) {
                      needCreate = findMatch = true;
                    } else if( child.code == item.split(' ')[0] ) { // function
                      child.code = item;
                      needCreate = findMatch = true;
                    }

                    if(needCreate) {
                      let itemHTML = $('<div class="' + settings.selectItemClass + '" ' + (bFunction ? ' data-type="function" ' : '') + "data-title='"+child.title+"' data-code='"+child.code+"' data-parent-index='"+parentIndex+"' data-child-index='"+childIndex+"'>"+child.title+"</div>");
                    
                      var dragBox = functions.createDrag(itemHTML);
                      if(dragBox) {
                        functions.insert(dragBox);
                      }
                    }

                  });
                }
              });

              if(!findMatch) {
                let itemHTML = $('<div class="' + settings.selectItemClass + ' ' + settings.selectTextBlockClass + '" ' + "data-code='' data-text='" + item + "'></div>");
              
                var dragBox = functions.createDrag(itemHTML);
                if(dragBox) {
                  functions.insert(dragBox);
                }
              }
            }
            settings.view = 'html';
          });
          console.log(inputText);
        }
      }
    };

    return functions;
  }

  function errorsCheck(settings, requieredProps) {
      var res = {
          result: true,
          message: '',
      };
      requieredProps.forEach(function(prop){
          if( settings[prop] === undefined || !settings[prop] ) {
              res.result = false;
              res.message = 'Не заполнен обязательный параметр ' + prop;
          }
      });
      return res;
  }
}
  
// объект на вход
var settings = {
    selectData: [
      {
        title: 'Функции',
        type: 'functions',
        childs: [
          {
            title: 'Нижний регистр',
            code: 'lower',
          }
        ]
      },
      {
          title: 'Поля раздела',
          childs: [
              {
                  title: 'Название текущего раздела',
                  code: 'this.Name',
              },
              {
                  title: 'Символьный код текущего раздела',
                  code: 'this.Code',
              },
              {
                  title: 'Описание текущего раздела',
                  code: 'this.PreviewText',
              },
          ]
      }
    ],
    textBlock: {
      title: 'Текстовый блок',
    },
};

// запуск основной функции
$('.main').asproTextConstruct(settings);